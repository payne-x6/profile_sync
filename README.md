# 1 Profile sync

Shell program that downloads and merge profile with my default settings for items listed in 1.3 Configure

## 1.1. Directories

 - data/	- configuration files
 - sync		- program for synchronization of profile

## 1.2. Runtime requirements

Software thats needed for proper execution

 - bash
 - curl
 - sdiff
 - tar
 - wget

## 1.3. Configures

 - bash
 - vim

# 2 Installation

## 2.1. Automatic installation

For automatic installation, run following line in terminal.

`sh -c "$(curl -fsSL https://bitbucket.org/payne-x6/profile_sync/downloads/downloader)"`

# 3 Copyright

Copyright 2020

## 3.1. Authors

 - Jan Hák (jan.hak@windowslive.com)

