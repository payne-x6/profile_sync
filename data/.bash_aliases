# Optimize video for Raspberry Pi using ffmpeg
rpi-ffmpeg () {
  CMD="ffmpeg "
  INPUT=0
  ACODEC=0
  VCODEC=0

  while [ $# -gt 1 ]; do
    case $1 in
      -i)
        [[ "$INPUT" -eq 1 && "$VCODEC" -eq 0 ]]  \
          && CMD="${CMD}-c:v libx265 "
        [[ "$INPUT" -eq 1 && "$ACODEC" -eq 0 ]]  \
          && CMD="${CMD}-c:a copy "
        ACODEC=0
        VCODEC=0
        CMD="${CMD}${1} "
        INPUT=1
        ;;
      -vcodec|-c:v)
        CMD="${CMD}${1} "
        VCODEC=1
        ;;
      -acodec|-c:a)
        CMD="${CMD}${1} "
        ACODEC=1
        ;;
      *)
        CMD="${CMD}${1} "
        ;;
    esac
    shift 1
  done

  [[ "$INPUT" -eq 1 && "$VCODEC" -eq 0 ]]  \
    && CMD="${CMD}-c:v libx265 "
  [[ "$INPUT" -eq 1 && "$ACODEC" -eq 0 ]]  \
    && CMD="${CMD}-c:a copy "
  ACODEC=0
  VCODEC=0
  
  eval $CMD $1

  unset CMD
  unset VCODEC
  unset ACODEC
  unset INPUT
}
