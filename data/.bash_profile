# MacOS
export BASH_SILENCE_DEPRECATION_WARNING=1

# All
export EDITOR=`which vim`
export VISUAL=$EDITOR
export LANG=cs_CZ

# Begin protected block
if [ -t 0 ]; then       # check for a terminal
  [ x"$TERM" = x"wy30" ] && stty erase ^h       # sample legacy environment
  echo "Logged as $LOGNAME at `date`"
fi

# End protected block
[ -r ~/.bashrc ] && source ~/.bashrc

