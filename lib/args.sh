#!/usr/bin/env sh

. ./lib/vector.sh

WITH="`vector_create bash vim`"

args_parse () {
  for arg in $@; do
    case $arg in
      --with-*)
        WITH="`vector_remove "$WITH" "${arg#--with-}"`"
	WITH="`vector_append "$WITH" "${arg#--with-}"`"
        ;;
      --without-*)
        echo $arg
        ;;
      *)
        echo "Unknown parameter $arg"
	;;
    esac
  done
  echo $WITH
}

