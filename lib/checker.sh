#!/usr/bin/env sh

check () {
  test -n "`which $1`" \
    && return 0 \
    || return 1 
}	

