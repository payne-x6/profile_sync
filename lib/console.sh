#!/usr/bin/env sh

FONT_NORMAL="\033[0m"
FONT_BOLD="\033[1m"
FONT_COLOR_LIGHT_GREEN="\033[92m"
FONT_COLOR_LIGHT_YELLOW="\033[93m"
FONT_COLOR_LIGHT_RED="\033[91m"

ECHO=`which echo`

cout () {
  $ECHO -n ""
}

text () {
  read stdin
  $ECHO -n -e "$stdin$1"
}

bold () {
  read stdin
  $ECHO -n -e "$stdin$FONT_BOLD"
}

lgreen () {
  read stdin
  $ECHO -n -e "$stdin$FONT_COLOR_LIGHT_GREEN"
}

lyellow () {
  read stdin
  $ECHO -n -e "$stdin$FONT_COLOR_LIGHT_YELLOW"
}

lred () {
  read stdin
  $ECHO -n -e "$stdin$FONT_COLOR_LIGHT_RED"
}

endl () {
  read stdin
  $ECHO -e "$stdin$FONT_NORMAL"
}

