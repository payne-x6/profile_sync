#!/usr/bin/env sh

. ./lib/console.sh

LOG_ERROR=0
LOG_WARNING=1
LOG_SUCCESS=2
LOG_INFO=3

log () {
  cout \
    | case "$1" in
      0) lred;;
      1) lyellow;;
      2) lgreen;;
      3) bold;;
    esac \
    | text "$2" \
    | endl
  test "$1" -eq "0" \
    && return 1 \
    || return 0
}

