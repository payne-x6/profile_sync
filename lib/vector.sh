#!/usr/bin/env sh

vector_create () {
  for arg; do
    echo -n "$arg:"
  done
}

vector_size () {
  echo -n "$1" | tr -cd ':' | wc -c
}

vector_set () {
  if test `vector_size "$1"` -ge $2; then
    echo -n "$1" | sed -r "s/[^:]+:/$3:/$2"
  else
    echo -n "$1$3:"
  fi
}

vector_append () {
  echo -n "$1"
  shift 1
  for arg; do
    echo -n "$arg:"
  done
}

vector_remove () {
  echo -n "$1" | sed -r -e "s/:$2:/:/g" -e "s/^$2://g"
}

vector_for_each () {
  test "$2" = "do" \
    || return 1
  test "$4" = "done" \
    || return 1
  ARR="$1"
  while test -n "$ARR" ; do
    CAR=${ARR%%:*}
    eval $3 "$CAR"
    ARR="${ARR#*:}"
  done
  unset CAR ARR
}

