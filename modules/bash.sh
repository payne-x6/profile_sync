#!/usr/bin/env sh

. ./lib/vector.sh

NAME="bash"

FILES="`vector_create \
  ".bashrc" \
  ".bash_profile" \
  ".bash_aliases"`"

merge () {
  log $LOG_INFO "    * Merging file $1"
  sdiff -d -o $TMP/build/$1 $TMP/data/$1 $PREFIX/$1
  test $? -eq 2 \
    && log $LOG_ERROR "Error while merging file '$1'" \
    && return 1
}

create () {
  log $LOG_INFO "    * Creating file $1"
  cp $TMP/data/$1 $TMP/build/$1
}

file_job () {
  mkdir -p `dirname $TMP/build/$1`
  if test -f $PREFIX/$1; then
    merge $1 || return 1
  else
    create $1 || return 1
  fi
}

file_install () {
  log $LOG_INFO "    * Installing file $1"
  mkdir -p `dirname $PREFIX/$1`
  mv $TMP/build/$1 $PREFIX/$1
}

module_prepare () {
  vector_for_each "$FILES" do file_job done
  return 0 
}

module_install () {
  vector_for_each "$FILES" do file_install done
  return 0
}

