# Tutorial

## Vim modes

## Commands

### Navigation

| key | function |
|:---:|:-------- |
|  h  | right    |
|  l  | left     |
|  j  | down     |
|  k  | up       |

It's possible to jump more symbols/lines with specify number *n* like `nx` where *x* is navigation key.

### Command basics

With `Vim` commands you will probably need these two functions...

|  key   | function |
|:------:|:-------- |
|   u    | undo     |
| Ctrl+r | redo     |
|   .    | repeat   |

Real text manipulation comes with this...

| key | function        | description                                                 |
|:---:|:--------------- |:----------------------------------------------------------- |
|  c  | change          | Delete object and go to `insert` mode                       |
|  d  | cut (delete)    | Cut object from text (also possible to delete text with it) |
|  p  | paste           | Paste                                                       |
|  v  | visually select | Visually select object                                      |
|  y  | yank            | Copy object                                                 |
|  <  | indent left     | Remove indentation                                          |
|  >  | indent right    | Add indentation                                             |

## Cheat sheet
